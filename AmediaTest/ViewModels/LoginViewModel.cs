﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AmediaTest.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Debe ingresar el usuario.")]
        [Display(Name = "Usuario")]
        public string Usuario { get; set; }
        [Required(ErrorMessage = "Debe ingresar la contraseña.")]
        [DataType(DataType.Password)]
        public string Contraseña { get; set; }
    }
}
