using AmediaTest.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmediaTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkSqlServer()
.AddDbContext<TestCRUDContext>(options =>
options.UseSqlServer("Data Source=W2K12MIGRACIOND;Initial Catalog=TestCRUD;User ID = sistemas; Password = jav51pal23"));


            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(
    options =>
    {
        options.LoginPath = "/Cuenta/Login";
        options.Cookie.Name = "TestCRUD";
        options.LogoutPath = "/Cuenta/Logout";
        options.AccessDeniedPath = "/Cuenta/AccesoDenegado";
        options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
        options.Cookie.Name = "TEST";
        options.Cookie.HttpOnly = true;
        options.ExpireTimeSpan = TimeSpan.FromDays(60);
    }
);

            services.AddControllersWithViews().AddRazorRuntimeCompilation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Cuenta}/{action=Login}/{id?}");
            });


            var db = new SqlConnection("Data Source=W2K12MIGRACIOND;Initial Catalog=TestCRUD;User ID = sistemas; Password = jav51pal23");

            //sqlite_conn.Open();

            //var db = new SqliteCommand(databasePath);

            var sql = (@"
DROP TABLE tGeneroPelicula
DROP TABLE tUsers
DROP TABLE tRol
DROP TABLE tGenero
DROP TABLE tPelicula
CREATE TABLE tRol
(
cod_rol INT IDENTITY PRIMARY KEY
, txt_desc VARCHAR(500)
, sn_activo INT
)
INSERT INTO trol VALUES ( 'Administrador',-1)
INSERT INTO trol VALUES ( 'Cliente', -1)
CREATE TABLE tUsers
(cod_usuario INT PRIMARY KEY IDENTITY, txt_user VARCHAR(50), txt_password
VARCHAR(50),txt_nombre VARCHAR(200), txt_apellido VARCHAR(200), nro_doc
VARCHAR(50), cod_rol INT, sn_activo INT
, CONSTRAINT fk_user_rol FOREIGN KEY (cod_rol) REFERENCES tRol(cod_rol)
)
INSERT INTO tUsers VALUES ( 'Admin', 'PassAdmin123', 'Administrador', 'Test', '1234321', 1,-1)
INSERT INTO tUsers VALUES ('userTest', 'Test1', 'Ariel', 'ApellidoConA', '12312321', 2, -1)
INSERT INTO tUsers VALUES ('userTest2', 'Test2', 'Bernardo', 'ApellidoConB', '12312322', 2, -1)
INSERT INTO tUsers VALUES ('userTest3', 'Test3', 'Carlos', 'ApellidoConC', '12312323', 2, -1)
CREATE TABLE tPelicula (cod_pelicula INT PRIMARY KEY IDENTITY, txt_desc
VARCHAR(500), cant_disponibles_alquiler INT, cant_disponibles_venta INT,
precio_alquiler NUMERIC(18,2), precio_venta NUMERIC(18,2))
INSERT INTO tPelicula VALUES ('Duro de matar III', 3, 0,1.5,5.0)
INSERT INTO tPelicula VALUES ('Todo Poderoso', 2,1,1.5,7.0)
INSERT INTO tPelicula VALUES ('Stranger than fiction', 1,1,1.5,8.0)
INSERT INTO tPelicula VALUES ('OUIJA', 0,2,2.0,20.50)
CREATE TABLE tGenero (cod_genero INT PRIMARY KEY IDENTITY, txt_desc
VARCHAR(500) )
INSERT INTO tGenero VALUES('Acci�n')
INSERT INTO tGenero VALUES('Comedia')
INSERT INTO tGenero VALUES('Drama')
INSERT INTO tGenero VALUES('Terror')
CREATE TABLE tGeneroPelicula (cod_pelicula INT, cod_genero INT
, PRIMARY KEY(cod_pelicula, cod_genero)
, CONSTRAINT fk_genero_pelicula FOREIGN KEY(cod_pelicula) REFERENCES
tpelicula(cod_pelicula)

, CONSTRAINT fk_pelicula_genero FOREIGN KEY(cod_genero) REFERENCES
tGenero(cod_genero))
INSERT INTO tGeneroPelicula VALUES(1,1)
INSERT INTO tGeneroPelicula VALUES(2,2)
INSERT INTO tGeneroPelicula VALUES(3,2)
INSERT INTO tGeneroPelicula VALUES(3,3)
INSERT INTO tGeneroPelicula VALUES(4,4)");

            var command = new SqlCommand(sql, db);

            db.Open();

            command.ExecuteNonQuery();

            var sqlStoreProcedure = @"CREATE PROCEDURE AltaUsuario (@id            INTEGER,
                                          @user    VARCHAR(50),
                                          @pass     VARCHAR(50),
										  @nombre     VARCHAR(200),
										  @apellido     VARCHAR(200),
										  @documento     VARCHAR(50),
										  @rol				int,
                                          @activo        int)
AS
  BEGIN
        BEGIN
            INSERT INTO dbo.tUsers VALUES(@user,@pass,@nombre,@apellido,@documento,@rol,@activo)
        END
  END";
            var createStoreProcedure = new SqlCommand(sqlStoreProcedure, db);


            command.ExecuteNonQuery();
        }
    }
}
