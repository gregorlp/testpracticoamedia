﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AmediaTest.Models;

namespace AmediaTest.Views.Users
{
    public class TUsersController : Controller
    {
        private readonly TestCRUDContext _context;

        public TUsersController(TestCRUDContext context)
        {
            _context = context;
        }

        // GET: TUsers
        public async Task<IActionResult> Index()
        {
            var testCRUDContext = _context.TUsers.Include(t => t.CodRolNavigation);
            return View(await testCRUDContext.ToListAsync());
        }

        // GET: TUsers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tUser = await _context.TUsers
                .Include(t => t.CodRolNavigation)
                .FirstOrDefaultAsync(m => m.CodUsuario == id);
            if (tUser == null)
            {
                return NotFound();
            }

            return View(tUser);
        }

        // GET: TUsers/Create
        public IActionResult Create()
        {
            ViewData["CodRol"] = new SelectList(_context.TRols, "CodRol", "CodRol");
            return View();
        }

        // POST: TUsers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CodUsuario,TxtUser,TxtPassword,TxtNombre,TxtApellido,NroDoc,CodRol,SnActivo")] TUser tUser)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CodRol"] = new SelectList(_context.TRols, "CodRol", "CodRol", tUser.CodRol);
            return View(tUser);
        }

        // GET: TUsers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tUser = await _context.TUsers.FindAsync(id);
            if (tUser == null)
            {
                return NotFound();
            }
            ViewData["CodRol"] = new SelectList(_context.TRols, "CodRol", "CodRol", tUser.CodRol);
            return View(tUser);
        }

        // POST: TUsers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CodUsuario,TxtUser,TxtPassword,TxtNombre,TxtApellido,NroDoc,CodRol,SnActivo")] TUser tUser)
        {
            if (id != tUser.CodUsuario)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TUserExists(tUser.CodUsuario))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CodRol"] = new SelectList(_context.TRols, "CodRol", "CodRol", tUser.CodRol);
            return View(tUser);
        }

        // GET: TUsers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tUser = await _context.TUsers
                .Include(t => t.CodRolNavigation)
                .FirstOrDefaultAsync(m => m.CodUsuario == id);
            if (tUser == null)
            {
                return NotFound();
            }

            return View(tUser);
        }

        // POST: TUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tUser = await _context.TUsers.FindAsync(id);
            _context.TUsers.Remove(tUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TUserExists(int id)
        {
            return _context.TUsers.Any(e => e.CodUsuario == id);
        }
    }
}
