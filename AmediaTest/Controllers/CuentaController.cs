﻿using AmediaTest.Models;
using AmediaTest.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AmediaTest.Controllers
{
    public class CuentaController : Controller
    {
        private const string Ruta = "Views/Login/";
        private readonly TestCRUDContext _dbContext;

        public CuentaController(TestCRUDContext dbContext)
        {
            _dbContext = dbContext;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View($"{Ruta}Login.cshtml");
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var usuario = _dbContext.TUsers.FirstOrDefault(x => x.TxtUser == loginViewModel.Usuario);

                if (usuario.TxtPassword == loginViewModel.Contraseña)
                {
                    var claims = new List<Claim>
                        {
                            new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", usuario.TxtUser),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };

                    var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);


                    var principal = new ClaimsPrincipal(identity);

                    await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme,
                        principal);

                    if (usuario.CodRol == 1)
                        return RedirectToAction("Index", "TUsers");
                    else
                        return RedirectToAction("Index", "Home");
                }

                else
                {
                    ModelState.AddModelError("", "Usuario o Contraseña invalidos");
                }

            }
            return View($"{Ruta}Login.cshtml", loginViewModel);
        }

        public async Task<IActionResult> Logout()
        {

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login","Cuenta");
        }
    }
}

